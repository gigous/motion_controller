Assumption for simplicity: client responsible for managing memory

Lessons learned:
  - Larger classes could have been broken down more
  - `std::launch::async|std::launch::deferred` allows future to run on a new thread, which can finish executing through the future's `wait()`
  - `this` in constructor won't point correctly if new isn't used (CommandProcessor and getting notified as observer)
