#pragma once

#include <iostream>
#include <future>
#include "MotionControllerCommandAsyncBase.hpp"

class CommitOutputLineChangesCommand : public MotionControllerCommandAsyncBase<void>
{
public:
    CommitOutputLineChangesCommand(IMotionController* motionController);
    virtual ~CommitOutputLineChangesCommand();
    virtual void Execute();
};
