#pragma once

#include <iostream>
#include <cmath>
#include <exception>
#include <future>
#include <thread>
#include <chrono>
#include <stdlib.h>
#include "interfaces/IMotor.h"
#include "interfaces/IMotionController.h"
#include "types/CallbackFunc.h"

typedef std::chrono::high_resolution_clock Time;
typedef std::chrono::milliseconds ms;
typedef std::chrono::nanoseconds ns;

class MotionController : public IMotionController
{
public:
    MotionController(IMotor* xMotor, IMotor* yMotor);
    virtual ~MotionController();
    virtual void Connect(CallbackFunc pFunc);
    virtual void SetOutputLine(int lineNumber);
    virtual void ClearOutputLine(int lineNumber);
    virtual void CommitOutputLineChanges();
    virtual void MoveTo(double xDistance, double yDistance, double speed);
    virtual void Dwell(int msecs);

protected:
    IMotor* _xMotor;
    IMotor* _yMotor;
    CallbackFunc _pFunc;
    unsigned char _digitalOutput;

private:
    bool outputLineWithinBounds(int lineNumber);
};