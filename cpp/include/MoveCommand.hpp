#pragma once

#include <iostream>
#include "interfaces/IMotionController.h"
#include "MotionControllerCommandAsyncBase.hpp"

class MoveCommand : public MotionControllerCommandAsyncBase<void>
{
public:
    MoveCommand(IMotionController* motionController, double xDistance, double yDistance, double speed);
    virtual ~MoveCommand();
    virtual void Execute();
private:
    double _xDistance;
    double _yDistance;
    double _speed;
};