#pragma once

#include "interfaces/ICommand.h"
#include "interfaces/IMotionController.h"

class MotionControllerCommandBase : public ICommand
{
public:
    MotionControllerCommandBase(IMotionController* motionController);
    virtual ~MotionControllerCommandBase();
    virtual void Execute() = 0;

protected:
    IMotionController* _motionController;
};