#pragma once

#include <iostream>
#include "interfaces/IMotionController.h"
#include "MotionControllerCommandAsyncBase.hpp"

class WaitCommand : public MotionControllerCommandAsyncBase<void>
{
public:
    WaitCommand(IMotionController* motionController, int msecs);
    virtual ~WaitCommand();
    virtual void Execute();
private:
    int _msecs;
};
