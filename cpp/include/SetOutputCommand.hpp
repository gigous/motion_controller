#pragma once

#include <iostream>
#include "interfaces/IMotionController.h"
#include "MotionControllerCommandBase.hpp"

class SetOutputCommand : public MotionControllerCommandBase
{
public:
    SetOutputCommand(IMotionController* motionController, int lineNumber);
    virtual ~SetOutputCommand();
    virtual void Execute();
private:
    int _lineNumber;
};
