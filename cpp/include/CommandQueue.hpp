#pragma once

#include <list>
#include <queue>
#include "interfaces/ICommand.h"
#include "interfaces/ICommandQueue.h"
#include "interfaces/IObserver.h"
#include "interfaces/ISubject.h"

template <typename T>
class CommandQueue : public ICommandQueue, public ISubject
{
    using iter = typename T::iterator;
public:
    // CommandQueue(CommandQueue&& other);
    // CommandQueue(const CommandQueue& other);
    CommandQueue(T commands);
    virtual ~CommandQueue();
    virtual std::string GetNext();
    virtual bool HasNext() const;
    virtual void Extend(T commands);
    virtual void RegisterObserver(IObserver* observer);
    virtual void RemoveObserver(IObserver* observer);
    virtual void NotifyObservers();

private:
    std::queue<std::string, T> _commands;
    std::list<IObserver*> _observers;
};

#include "../src/CommandQueue.tpp"
