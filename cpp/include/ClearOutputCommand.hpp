#pragma once

#include <iostream>
#include "MotionControllerCommandBase.hpp"

class ClearOutputCommand : public MotionControllerCommandBase
{
public:
    ClearOutputCommand(IMotionController* motionController, int lineNumber);
    virtual ~ClearOutputCommand();
    virtual void Execute();
private:
    int _lineNumber;
};
