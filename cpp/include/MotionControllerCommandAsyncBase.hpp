#pragma once

#include <future>
#include <iostream>
#include "MotionControllerCommandBase.hpp"
#include "interfaces/ITaskHandler.h"

template <typename T>
class MotionControllerCommandAsyncBase : public ITaskHandler<T>, public MotionControllerCommandBase
{
public:
    MotionControllerCommandAsyncBase(IMotionController* motionController);
    virtual ~MotionControllerCommandAsyncBase();
    virtual std::future<T>* GetExecutingTask();

protected:
    std::future<T> _task;
};

#include "../src/MotionControllerCommandAsyncBase.tpp"
