#pragma once

#include <functional>

// Due to better flexibility with std::function as a function pointer,
// I chose it over the C-style version
typedef std::function<void(void*)> CallbackFunc;
