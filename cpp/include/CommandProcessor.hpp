#pragma once

#include <future>
#include <iostream>
#include <thread>
#include <mutex>
#include <string>
#include <chrono>
#include <vector>
#include <memory>
#include <boost/algorithm/string.hpp>
#include "interfaces/IObserver.h"
#include "interfaces/ISubject.h"
#include "interfaces/ICommandProcessor.h"
#include "interfaces/ICommandQueue.h"
#include "interfaces/ICommandFactory.h"
#include "interfaces/ICallbackHost.h"
#include "interfaces/IMotionController.h"
#include "interfaces/ITaskHandler.h"
#include "types/CallbackFunc.h"
#include "MoveCommand.hpp"
#include "WaitCommand.hpp"
#include "SetOutputCommand.hpp"
#include "ClearOutputCommand.hpp"
#include "CommitOutputLineChangesCommand.hpp"

// TODO This class could be broken down into other classes based on Single Responsibility Principle

class CommandProcessor : public IObserver, public ICommandProcessor, private ICommandFactory, private ICallbackHost
{
public:
    CommandProcessor(ICommandQueue* queue, IMotionController* motionContoller, ISubject* subject);
    // copy constructor
    CommandProcessor(const CommandProcessor& other);
    // move constructor
    CommandProcessor(CommandProcessor&& other);
    // destructor
    virtual ~CommandProcessor();
    // copy assignment operator
    virtual CommandProcessor& operator=(CommandProcessor other);
    // move assignment operator
    virtual CommandProcessor& operator=(CommandProcessor&& other);
    friend void swap(CommandProcessor& first, CommandProcessor& second); //noexcept
    virtual void Update();
    virtual std::unique_ptr<ICommand> CreateCommand(std::string command);
    virtual void StartProcessing();
    virtual void StopProcessing();
    virtual std::string GetLastCommandExecuted();
    virtual void WaitUntilDone();
protected:
    CallbackFunc _commandCompleteCallback;
    const std::chrono::milliseconds COMMAND_LOOP_IDLE_SLEEP_MSECS = std::chrono::milliseconds(25);
    const std::chrono::milliseconds COMMAND_LOOP_WAIT_SLEEP_MSECS = std::chrono::milliseconds(25);
    virtual void SetLastCommandExecuted(std::string lastCommand);
private:
    // shared resources between threads
    bool _started;
    bool _idle;
    bool _running;
    // TODO Make custom class for command strings
    std::string _lastCommandExecuted;

    // objects
    ICommandQueue* _queue;
    IMotionController* _motionController;

    // threads and locks
    std::thread _commandThread;
    std::mutex _mutex;

    // methods
    double paramToDouble(std::string param);
    int paramToInt(std::string param);
    void setIdleMode();
    void commandLoop();
    void waitForAsyncCommand(std::future<void>* task);
    void CommandCompleteCallback(void* data);
};
