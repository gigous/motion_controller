#pragma once

#include <stdexcept>
#include "interfaces/IMotor.h"

class Motor : public IMotor
{
public:
    Motor(double lowerLimit, double upperLimit, double position);
    virtual ~Motor();
    virtual void Move(double steps);
    virtual double GetPosition() const;

private:
    double _lowerLimit;
    double _upperLimit;
    double _position;
};