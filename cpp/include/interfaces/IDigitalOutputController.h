#pragma once

class IDigitalOutputController
{
public:
    virtual ~IDigitalOutputController(){};
    virtual void SetOutputLine(int lineNumber) = 0;
    virtual void ClearOutputLine(int lineNumber) = 0;
    virtual void CommitOutputLineChanges() = 0;
};
