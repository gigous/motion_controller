#pragma once

#include "IMotor.h"
#include "IDigitalOutputController.h"
#include "../types/CallbackFunc.h"

class IMotionController : public IDigitalOutputController
{
public:
    virtual void Connect(CallbackFunc pFunc) = 0;
    virtual void MoveTo(double xDistance, double yDistance, double speed) = 0;
    virtual void Dwell(int msecs) = 0;
};