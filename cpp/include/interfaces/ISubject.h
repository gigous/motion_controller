#pragma once

#include "IObserver.h"

class ISubject
{
public:
    virtual ~ISubject(){};
    virtual void RegisterObserver(IObserver* observer) = 0;
    virtual void RemoveObserver(IObserver* observer) = 0;
    virtual void NotifyObservers() = 0;
};
