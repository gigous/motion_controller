#pragma once

class ICallbackHost
{
public:
    ~ICallbackHost(){};
    void CommandCompleteCallback(void * data);
};
