#pragma once

#include <string>

class ICommandProcessor
{
public:
    virtual ~ICommandProcessor(){};
    virtual void StartProcessing() = 0;
    virtual void StopProcessing() = 0;
    virtual std::string GetLastCommandExecuted() = 0;
    virtual void WaitUntilDone() = 0;
protected:
    virtual void SetLastCommandExecuted(std::string lastCommand) = 0;
};