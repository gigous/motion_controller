#pragma once

#include <future>

template <typename T>
class ITaskHandler
{
public:
    virtual ~ITaskHandler(){};
    virtual std::future<T>* GetExecutingTask() = 0;
};