#pragma once

class IMotor
{
public:
    virtual ~IMotor(){};
    // assuming for simplicity 1 step translates to 1 inch
    virtual void Move(double steps) = 0;
    virtual double GetPosition() const = 0;
};