#pragma once

#include <string>
#include <memory>
#include "ICommand.h"

class ICommandFactory
{
public:
    virtual ~ICommandFactory(){};
    virtual std::unique_ptr<ICommand> CreateCommand(std::string command) = 0;
};
