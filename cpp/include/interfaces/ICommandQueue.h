#pragma once

#include <string>

class ICommandQueue
{
public:
    virtual ~ICommandQueue(){};
    virtual std::string GetNext() = 0;
    virtual bool HasNext() const = 0;
};
