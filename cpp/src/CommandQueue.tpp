#pragma once

template <typename T>
CommandQueue<T>::CommandQueue(T commands)
    : _commands(commands), _observers()
{
}

template <typename T>
CommandQueue<T>::~CommandQueue() { }

template <typename T>
std::string CommandQueue<T>::GetNext()
{
    auto c = _commands.front();
    _commands.pop();
    return c;
}

template <typename T>
bool CommandQueue<T>::HasNext() const
{
    return !_commands.empty();
}

template <typename T>
void CommandQueue<T>::Extend(T commands)
{
    for (auto it = commands.begin(); it != commands.end(); it++)
    {
        _commands.push(*it);
    }
    if (!commands.empty())
        // new commands added, so notify observers
        NotifyObservers();
}

template <typename T>
void CommandQueue<T>::RegisterObserver(IObserver* observer)
{
    _observers.push_back(observer);
}

template <typename T>
void CommandQueue<T>::RemoveObserver(IObserver* observer)
{
    _observers.remove(observer);
}

template <typename T>
void CommandQueue<T>::NotifyObservers()
{
    for (auto it = _observers.begin(); it != _observers.end(); it++)
    {
        (*it)->Update();
    }
}
