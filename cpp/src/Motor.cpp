#include "../include/Motor.hpp"

Motor::Motor(double lowerLimit, double upperLimit, double position)
    : _lowerLimit(lowerLimit), _upperLimit(upperLimit), _position(position)
{
    if (_lowerLimit < 0.0)
        throw std::runtime_error("Motor lower limit cannot be negative");
    else if (_upperLimit <= _lowerLimit)
        throw std::runtime_error("Motor upper limit cannot be lower than lower limit");
    else if (_position < _lowerLimit)
        throw std::runtime_error("Motor starting position cannot be lower than lower limit");
    else if (_position > _upperLimit)
        throw std::runtime_error("Motor starting position cannot be higher than upper limit");
}

Motor::~Motor(){}

void Motor::Move(double steps)
{
    // move motor to new position and prevent it from going over limits
    double newPos = _position + steps;
    if (steps < 0.0)
        _position = newPos > _lowerLimit ? newPos : _lowerLimit;
    else
        _position = newPos < _upperLimit ? newPos : _upperLimit;
}

double Motor::GetPosition() const
{
    return _position;
}