#pragma once

template <typename T>
MotionControllerCommandAsyncBase<T>::MotionControllerCommandAsyncBase(IMotionController* motionController)
: MotionControllerCommandBase(motionController)
{ }

template <typename T>
MotionControllerCommandAsyncBase<T>::~MotionControllerCommandAsyncBase() {}

template <typename T>
std::future<T>* MotionControllerCommandAsyncBase<T>::GetExecutingTask()
{
    return &_task;
}
