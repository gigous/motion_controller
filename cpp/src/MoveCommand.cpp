#include "../include/MoveCommand.hpp"

MoveCommand::MoveCommand(IMotionController* motionController, double xDistance, double yDistance, double speed)
: MotionControllerCommandAsyncBase(motionController), _xDistance(xDistance), _yDistance(yDistance), _speed(speed) {}

MoveCommand::~MoveCommand(){};

void MoveCommand::Execute()
{
    _task = std::async(std::launch::async|std::launch::deferred, &IMotionController::MoveTo, _motionController, _xDistance, _yDistance, _speed);
}