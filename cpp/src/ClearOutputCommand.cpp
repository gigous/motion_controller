#include "../include/ClearOutputCommand.hpp"

ClearOutputCommand::ClearOutputCommand(IMotionController* motionController, int lineNumber)
: MotionControllerCommandBase(motionController), _lineNumber(lineNumber) {}

ClearOutputCommand::~ClearOutputCommand() {}

void ClearOutputCommand::Execute()
{
    _motionController->ClearOutputLine(_lineNumber);
}