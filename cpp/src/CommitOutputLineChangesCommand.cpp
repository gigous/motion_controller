#include "../include/CommitOutputLineChangesCommand.hpp"

CommitOutputLineChangesCommand::CommitOutputLineChangesCommand(IMotionController* motionController)
    : MotionControllerCommandAsyncBase(motionController)
{ }

CommitOutputLineChangesCommand::~CommitOutputLineChangesCommand() {}

void CommitOutputLineChangesCommand::Execute()
{
    _task = std::async(std::launch::async|std::launch::deferred, &IMotionController::CommitOutputLineChanges, _motionController);
}