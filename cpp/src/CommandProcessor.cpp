#include "../include/CommandProcessor.hpp"

CommandProcessor::CommandProcessor(ICommandQueue* queue, IMotionController* motionContoller, ISubject* subject)
    : _started(false), _idle(false), _running(false), _lastCommandExecuted(),
    _queue(queue), _motionController(motionContoller), _commandThread(), _mutex()
{
    // bind to this processor's command complete callback function,
    // also prevent usually hidden "this" parameter from needed to be passed in on the motion controller side
    CallbackFunc pFunc = std::bind(&CommandProcessor::CommandCompleteCallback, this, std::placeholders::_1);
    _motionController->Connect(pFunc);
    // assuming processor doesn't need to remove itself, so don't save subject pointer
    subject->RegisterObserver(this);
}

CommandProcessor::CommandProcessor(CommandProcessor&& other)
{
    swap(*this, other);
}

void swap(CommandProcessor& first, CommandProcessor& second) // noexcept
{
    std::lock_guard<std::mutex> firstLock(first._mutex, std::adopt_lock);
    std::lock_guard<std::mutex> secondLock(second._mutex, std::adopt_lock);

    using std::swap;

    swap(first._started, second._started);
    swap(first._idle, second._idle);
    swap(first._running, second._running);
    swap(first._lastCommandExecuted, second._lastCommandExecuted);
    swap(first._queue, second._queue);
    swap(first._motionController, second._motionController);
    swap(first._commandThread, second._commandThread);
}

CommandProcessor& CommandProcessor::operator=(CommandProcessor other)
{
    swap(*this, other);
    return *this;
}

CommandProcessor& CommandProcessor::operator=(CommandProcessor&& other)
{
    swap(*this, other);
    return *this;
}

CommandProcessor::CommandProcessor(const CommandProcessor& other)
{
    operator=(other);
}

void CommandProcessor::CommandCompleteCallback(void * data)
{
    if (data != nullptr)
    {
        // data was passed in,
        // cast to the type of data and process it here
    }
    // command is complete
}

CommandProcessor::~CommandProcessor()
{
    if (_running || _idle)
        StopProcessing();
}

void CommandProcessor::StartProcessing()
{
    std::lock_guard<std::mutex> lock(_mutex);
    _started = true;
    _idle = false;
    _running = true;
    // Start new thread to process commands
    _commandThread = std::thread(&CommandProcessor::commandLoop, this);
}

void CommandProcessor::StopProcessing()
{
    std::lock_guard<std::mutex> lock(_mutex);
    _idle = false;
    _running = false;
    if (_commandThread.joinable())
        // wait for any working operation to finish
        _commandThread.join();
}

std::string CommandProcessor::GetLastCommandExecuted()
{
    std::lock_guard<std::mutex> lock(_mutex);
    return _lastCommandExecuted;
}

void CommandProcessor::WaitUntilDone()
{
    while (_running)
    {
        std::this_thread::sleep_for(COMMAND_LOOP_WAIT_SLEEP_MSECS);
    }
}

void CommandProcessor::SetLastCommandExecuted(std::string lastCommand)
{
    std::lock_guard<std::mutex> lock(_mutex);
    _lastCommandExecuted = lastCommand;
}

void CommandProcessor::Update()
{
    // CommandProcessor is notified that more commands
    // can be pulled from the queue, go back to running state
    std::lock_guard<std::mutex> lock(_mutex);
    _running = true;
    _idle = false;
}

void CommandProcessor::setIdleMode()
{
    // Set to idle mode until new commands are added
    std::lock_guard<std::mutex> lock(_mutex);
    _running = false;
    _idle = true;
}

void CommandProcessor::waitForAsyncCommand(std::future<void>* task)
{
    // Block until async command is done
    task->wait();
}

void CommandProcessor::commandLoop()
{
    while (_running || _idle)
    {
        while (_running && !_idle)
        {
            if (!_queue->HasNext())
            {
                // no more commands to process
                setIdleMode();
                break;
            }
            // get the next command from the queue
            std::string nextCommand = _queue->GetNext();
            // pass the command string to the command factory method
            std::unique_ptr<ICommand> command = CreateCommand(nextCommand);
            // If the command is asynchronous, the cast to task handler will succeed
            // otherwise the pointer is null
            ITaskHandler<void>* taskHandler = dynamic_cast<ITaskHandler<void>*>(command.get());
            // Execute command on the motion controller
            command->Execute();
            if (taskHandler != nullptr)
                // wait for the asynchronous command to complete
                waitForAsyncCommand(taskHandler->GetExecutingTask());
                // optionally, other things can be done while waiting for the command
            SetLastCommandExecuted(nextCommand);
        }
        while (!_running && _idle)
        {
            // Idle mode, keep waiting until new commands are posted
            std::this_thread::sleep_for(COMMAND_LOOP_IDLE_SLEEP_MSECS);
        }
    }
}

std::unique_ptr<ICommand> CommandProcessor::CreateCommand(std::string command)
{
    std::vector<std::string> results;
    // split commands with space as delimiter
    boost::split(results, command, [](char c){return c == ' ';});
    if (results[0] == "Move")
    {
        float xDistance = paramToDouble(results[1]);
        float yDistance = paramToDouble(results[2]);
        float speed = paramToDouble(results[3]);
        return std::unique_ptr<MoveCommand>(new MoveCommand(_motionController, xDistance, yDistance, speed));
    }
    else if (results[0] == "Wait")
    {
        int msecs = paramToInt(results[1]);
        return std::unique_ptr<WaitCommand>(new WaitCommand(_motionController, msecs));
    }
    else if (results[0] == "CommitOutputLineChanges")
    {
        return std::unique_ptr<CommitOutputLineChangesCommand>(new CommitOutputLineChangesCommand(_motionController));
    }
    else if (results[0] == "SetOutput")
    {
        int lineNumber = paramToInt(results[1]);
        return std::unique_ptr<SetOutputCommand>(new SetOutputCommand(_motionController, lineNumber));
    }
    else if (results[0] == "ClearOutput")
    {
        int lineNumber = paramToInt(results[1]);
        return std::unique_ptr<ClearOutputCommand>(new ClearOutputCommand(_motionController, lineNumber));
    }
    else
    {
        // TODO make custom exceptions for errors
        throw std::runtime_error("Error: Command " + results[0] + " is invalid");
    }
}

double CommandProcessor::paramToDouble(std::string param)
{
    char* ending;
    double output = strtof(param.c_str(), &ending);
    return output;
}

int CommandProcessor::paramToInt(std::string param)
{
    int output = std::stoi(param.c_str());
    return output;
}
