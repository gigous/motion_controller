#include "../include/WaitCommand.hpp"

WaitCommand::WaitCommand(IMotionController* motionController, int msecs)
: MotionControllerCommandAsyncBase(motionController), _msecs(msecs) {}

WaitCommand::~WaitCommand() {}

void WaitCommand::Execute()
{
    _task = std::async(std::launch::async|std::launch::deferred, &IMotionController::Dwell, _motionController, _msecs);
}