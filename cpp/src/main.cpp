#include "CommandQueue.hpp"
#include "CommandProcessor.hpp"
#include "MotionController.hpp"
#include "Motor.hpp"

#include <iostream>
#include <list>

using namespace std;

int main()
{
    // Assumption for simplicity: client responsible for managing memory

    list<string> sl = {};
    sl.push_back("Move 1.2 2.3 3.4");
    sl.push_back("Wait 444");
    sl.push_back("SetOutput 5");
    sl.push_back("ClearOutput 5");
    sl.push_back("CommitOutputLineChanges");
    std::unique_ptr<CommandQueue<list<string> > > cq = std::unique_ptr<CommandQueue<list<string> > >(new CommandQueue<list<string> >(sl));
    std::unique_ptr<MotionController> mc;
    std::unique_ptr<CommandProcessor> cp;

    std::unique_ptr<Motor> yMotor = std::unique_ptr<Motor>(new Motor(0.0, 100.0, 50.0));
    std::unique_ptr<Motor> xMotor = std::unique_ptr<Motor>(new Motor(0.0, 100.0, 50.0));

    try
    {
        mc = std::unique_ptr<MotionController>(new MotionController(xMotor.get(), yMotor.get()));
        cp = std::unique_ptr<CommandProcessor>(new CommandProcessor(cq.get(), mc.get(), cq.get()));
        cp->StartProcessing();
        cp->WaitUntilDone();
        list<string> sl2 = {"Move 3.4 3.4 2.0", "Wait 443", "SetOutput 1"};
        cq->Extend(sl2);
        cp->WaitUntilDone();
    }
    catch (int e)
    {
        cout << e << endl;
    }
    catch (const std::exception& e)
    {
        cout << e.what() << std::endl;
    }
}