#include "../include/SetOutputCommand.hpp"

SetOutputCommand::SetOutputCommand(IMotionController* motionController, int lineNumber)
: MotionControllerCommandBase(motionController), _lineNumber(lineNumber) {}

SetOutputCommand::~SetOutputCommand() {}

void SetOutputCommand::Execute()
{
    _motionController->SetOutputLine(_lineNumber);
}