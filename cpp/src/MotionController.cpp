#include "../include/MotionController.hpp"

MotionController::MotionController(IMotor* xMotor, IMotor* yMotor)
    : _xMotor(xMotor), _yMotor(yMotor), _pFunc(), _digitalOutput(0)
{
}

MotionController::~MotionController() {}

void MotionController::Connect(CallbackFunc pFunc)
{
    _pFunc = pFunc;
}

void MotionController::MoveTo(double xDistance, double yDistance, double speed)
{
    long double totalDist = sqrt(pow(xDistance, 2.0) + pow(yDistance, 2.0));
    long double msecsLeft = totalDist/speed * 1000.0;
    long double stepX = xDistance/msecsLeft;
    long double stepY = yDistance/msecsLeft;
    long double deltaMsecs = 10.0;
    // Move motors in chunks to simulate movement over time
    // perform move, then sleep for remainder of time step
    while (msecsLeft > 0.0)
    {
        std::chrono::high_resolution_clock::time_point _startTime = Time::now();
        msecsLeft = msecsLeft > deltaMsecs ? msecsLeft - deltaMsecs : 0.0;
        if (msecsLeft <= 0) break;
        long double stepMsecs = (msecsLeft > 0.0 ? deltaMsecs : msecsLeft);
        _xMotor->Move(stepX * stepMsecs);
        _yMotor->Move(stepY * stepMsecs);
        auto elapsedTimeNsecs = std::chrono::duration_cast<ns>(Time::now() - _startTime);
        auto timeSleepNsecs = std::chrono::nanoseconds((int)round(stepMsecs * 1000)) - elapsedTimeNsecs;
        std::this_thread::sleep_for(timeSleepNsecs);
    }
    _pFunc(this);
}

void MotionController::Dwell(int msecs)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(msecs));
    _pFunc(this);
}

void MotionController::CommitOutputLineChanges()
{
    // Set output lines on hardware
    // use sleep to simulate operation
    auto r = rand() % 150;
    std::this_thread::sleep_for(std::chrono::milliseconds(r));
    _pFunc(this);
}

void MotionController::SetOutputLine(int lineNumber)
{
    if (!outputLineWithinBounds(lineNumber))
        throw std::runtime_error("Digital output line " + std::to_string(lineNumber) + "is invalid");
    // Perform bitwise AND of 0 only at line number position
    _digitalOutput |= (1 << lineNumber);
}

void MotionController::ClearOutputLine(int lineNumber)
{
    if (!outputLineWithinBounds(lineNumber))
        throw std::runtime_error("Digital output line " + std::to_string(lineNumber) + "is invalid");
    // Perform bitwise AND of 0 only at line number position
    _digitalOutput &= ~(1 << lineNumber);
}

bool MotionController::outputLineWithinBounds(int lineNumber)
{
    return lineNumber < 8 && lineNumber >= 0;
}