import logging
import math
from dataclasses import dataclass, field
from collections import defaultdict, deque
from time import sleep
from threading import Thread, Lock, current_thread
from functools import wraps
from typing import Final
from random import randint
import asyncio
# from prototype.processor import command_processor

def setup_logging(*args, **kwargs):
    from .setup_logging import setup_logging
    setup_logging(*args, **kwargs)
    logger = logging.getLogger(__name__)
    logger.info("======================= Logging started ================================")
    print('tried print start log')
    return logger

_LOGGER = setup_logging()

def _sleep_msecs(msecs: float):
    # _LOGGER.debug("Thread '%s' sleeping for %f", current_thread().name, msecs)
    sleep(msecs / 1000.0)

async def _async_sleep_msecs(msecs: float):
    # _LOGGER.debug("Thread '%s' sleeping for %f", current_thread().name, msecs)
    await asyncio.sleep(msecs / 1000.0)

@dataclass
class Motor:
    lower_limit: float = 0
    upper_limit: float = 100
    position: float = 50

    def __post_init__(self):
        assert self.lower_limit >= 0
        assert self.upper_limit > 0 and self.upper_limit > self.lower_limit
        assert self.lower_limit <= self.position <= self.upper_limit

    def move(self, steps):
        # add randomness to simulate real life
        # _sleep_msecs(randint(0, 10))
        new_pos = self.position + steps
        if steps < 0.0:
            self.position = new_pos if new_pos > self.lower_limit else self.lower_limit
        else:
            self.position = new_pos if new_pos < self.upper_limit else self.upper_limit


def _count_execution(m: callable) -> callable:
    @wraps(m)
    def execution_wrapper(*args, **kwargs):
        args[0]._executions[m.__name__] += 1
        return m(*args, **kwargs)
    return execution_wrapper


class MotionController:
    MOTOR_TIMESTEP_MSEC: Final = 10

    def __init__(self):
        # assumption: lines all 0 when motion controller init'd
        self.x_motor = Motor()
        self.y_motor = Motor()
        self.output_lines = 0x00
        self.pfunc: callable = None
        self._executions = defaultdict(int)

    def connect(self, pfunc: callable):
        self.pfunc = pfunc

    @_count_execution
    async def move_to(self, *args):
        _LOGGER.debug("Awaiting actual move_to...")
        await self._move_to(*args)

    async def _move_to(self, x_distance, y_distance, speed):
        # _LOGGER.debug("Threads: %s", str([t for t in threading.enumerate()]))
        _LOGGER.info(
            "Moving X Motor %f inches, Y Motor %f inches, at speed of %f inches/second",
            x_distance, y_distance, speed)
        total_dist = math.sqrt(x_distance**2 + y_distance**2)
        msecs_left = total_dist/speed * 1000
        step_x = x_distance/msecs_left
        step_y = y_distance/msecs_left
        delta_ms = 10
        while msecs_left > 0:
            msecs_left = msecs_left - delta_ms if msecs_left > delta_ms else 0
            st = delta_ms if msecs_left > 0 else msecs_left
            self.x_motor.move(step_x * delta_ms)
            self.y_motor.move(step_y * delta_ms)
            _async_sleep_msecs(st)
        x = f"{self.x_motor.position:.5f}"
        y = f"{self.y_motor.position:.5f}"
        _LOGGER.debug("Moved motors to new positions x: %s, y: %s", x, y)
        _LOGGER.debug("Calling callback")
        await self.pfunc(self.move_to)

    @_count_execution
    async def dwell(self, msecs):
        _LOGGER.debug("Awaiting actual dwell...")
        await self._dwell(msecs)

    async def _dwell(self, msecs):
        _LOGGER.debug("Dwelling for %f msecs", msecs)
        # TODO sleeping?
        asyncio.sleep(msecs / 1000)
        await self.pfunc(self.dwell)

    @_count_execution
    async def commit_output_line_changes(self):
        _LOGGER.debug("Awaiting actual commit_output_line_changes...")
        await self._commit_output_line_changes()

    async def _commit_output_line_changes(self):
        _LOGGER.debug("Committing output line changes: %s", f"{self.output_lines:#0x}")
        # TODO: do changes
        ...
        _LOGGER.debug("Calling callback")
        await self.pfunc(self.commit_output_line_changes)

    def _line_number_valid(self, line_number):
        # or 1-8?
        if line_number > 7 or line_number < 0:
            raise ValueError(f"Invalid output line number: {line_number}")

    @_count_execution
    def set_output_line(self, line_number: int):
        self._line_number_valid(line_number)
        self.output_lines |= (0x1 << line_number)

    @_count_execution
    def clear_output_line(self, line_number: int):
        self._line_number_valid(line_number)
        self.output_lines &= ~(0x1 << line_number)


class IObserver:
    def update(self):
        pass


class Observer(IObserver):
    pass


class ICommand:
    def execute(self):
        pass

class ITaskHandler:
    @property
    def task(self):
        pass

    @task.setter
    def task(self, value):
        pass

class NoCommand(ICommand):
    pass

@dataclass
class MotionControllerCommandBase(ICommand, ITaskHandler):
    motion_controller: MotionController
    def __init__(self, *args, **kwargs):
        self.task_ = None

    @property
    def task(self):
        return self.task_

    @task.setter
    def task(self, value):
        self.task_ = value

    def execute(self, *args, **kwargs):
        super().execute()
        _LOGGER.info(f"Executing command %s", self.__class__.__name__)
        if self.motion_controller is None:
            _LOGGER.warn(f"Did not execute %s as motion controller is None", self.__class__.__name__)
            return False


@dataclass
class MoveCommand(MotionControllerCommandBase):
    x_distance: float
    y_distance: float
    speed: float

    def execute(self):
        if super().execute() == False:
            return

        self.task = asyncio.ensure_future(
            self.motion_controller.move_to(
                self.x_distance,
                self.y_distance,
                self.speed
            )
        )


@dataclass
class WaitCommand(MotionControllerCommandBase):
    msecs: float

    def execute(self):
        if super().execute() == False:
            return

        self.task = asyncio.ensure_future(self.motion_controller.dwell(self.msecs))


@dataclass
class SetOutputCommand(MotionControllerCommandBase):
    line_number: int

    def execute(self):
        if super().execute() == False:
            return

        # synchronous
        self.task = self.motion_controller.set_output_line(self.line_number)


@dataclass
class ClearOutputCommand(MotionControllerCommandBase):
    line_number: int

    def execute(self):
        if super().execute() == False:
            return

        # synchronous
        self.task = self.motion_controller.clear_output_line(self.line_number)


class ISubject:
    def register_observer(self, observer):
        pass

    def notify_observers(self):
        pass


class Subject(ISubject):
    def __init__(self):
        self.observers = []

    def register_observer(self, observer):
        super().register_observer(observer)
        _LOGGER.info(f"Registering observer %s with subject", observer.__class__.__name__)
        self.observers.append(observer)

    def remove_observer(self, observer):
        self.observers.remove(observer)

    def notify_observers(self):
        super().notify_observers()
        _LOGGER.info("Notifying observers")
        for o in self.observers:
            o.update()

# TODO? CommandParser class (separation of resps)
# Could be extra because don't need to read from file
class CommandQueue(Subject):
    def __init__(self, commands):
        super().__init__()
        self.commands = deque(commands)

    def get_next(self):
        if not self.commands:
            return None
        _LOGGER.debug(f"Returning next command %s", command := self.commands.popleft())
        return command

    # How would this work, adding commands to the queue?
    def add_more_commands(self, commands):
        # May need lock
        _LOGGER.debug(f"Adding more commands %s", str(commands))
        self.commands.extendleft(commands)
        self.notify_observers()


def _start_required(m) -> callable:
    @wraps(m)
    def start_wrapper(*args, **kwargs):
        if not args[0]._started:
            raise RuntimeError("Command Processor must be started first")
        return m(*args, **kwargs)
    return start_wrapper

class CommandProcessor(Observer):
    _id = 0
    COMMAND_LOOP_IDLE_SLEEP_MSECS: Final = 10

    # Does it make sense to pass motion controller here?
    # Think queue is justified as it is Observer pattern
    def __init__(self, queue: CommandQueue, motion_controller: MotionController, pause_between_commands_msecs=0):
        super().__init__()
        _LOGGER.debug("Creating %s", self.__class__.__name__)

        # shared resources
        self._started = False
        self._running = False
        self._executing = False
        self._idle = False
        self._update = False
        self._last_command_executed = None
        self._current_command = None
        self._future = None
        self._loop = asyncio.new_event_loop()

        # threads and locks
        self.command_thread = Thread(target=self._command_loop, name=f"CommandLoop-{self._id}")
        self.command_lock = Lock()

        # objects
        # NOTE: composition > inheritance
        self.queue = queue
        self.queue.register_observer(self)
        self.motion_controller = motion_controller
        self.motion_controller.connect(self.command_complete)

        # other variables
        self._pause_between_commands_msecs = pause_between_commands_msecs
        self._commands_processed = 0

        # class variables
        self._id += 1

    def __del__(self):
        _LOGGER.debug(f"%s.__del__ called", self.__class__.__name__)
        if self._running or self._idle:
            self.stop_processing()

    def start_processing(self):
        _LOGGER.info("Start processing commands")
        with self.command_lock:
            self._started = True
            self._running = True
            self._idle = False
        _LOGGER.debug("Starting thread '%s'", self.command_thread.name)
        self.command_thread.start()
        _LOGGER.debug("Started thread '%s'", self.command_thread.name)

    def stop_processing(self):
        _LOGGER.debug("Stopping command processing")
        if not self.command_thread.is_alive():
            _LOGGER.warn("Processing already stopped for thread '%s'", self.command_thread.name)
            return
        with self.command_lock:
            self._idle = False
            self._running = False
        _LOGGER.debug("Joining thread '%s'", self.command_thread.name)
        self.command_thread.join()
        _LOGGER.debug("Thread '%s' joined", self.command_thread.name)
        _LOGGER.info("Stopped processing commands")

    @_start_required
    def update(self):
        _LOGGER.debug("Updating %s that commands added", self.__class__.__name__)
        with self.command_lock:
            self._update = True
            self._running = True
            self._idle = False
        _LOGGER.debug("Updated %s", self.__class__.__name__)

    async def command_complete(self, o: object):
        self.last_command_executed = self._current_command
        with self.command_lock:
            self._executing = False
        loop = asyncio.get_running_loop()
        loop.stop()
        _LOGGER.info("Async command %s has finished executing, return value %s", self.last_command_executed, o)

    @property
    @_start_required
    def last_command_executed(self):
        with self.command_lock:
            # _LOGGER.debug("Query last command executed: returning '%s'", self._last_command_executed)
            return self._last_command_executed

    @last_command_executed.setter
    @_start_required
    def last_command_executed(self, value):
        with self.command_lock:
            self._last_command_executed = value

    def _set_idle(self):
        _LOGGER.debug(f"No more commands for %s to process", self.__class__.__name__)
        if not self._started:
            _LOGGER.warn("%s._started is False when setting idle mode", self.__class__.__name__)
        with self.command_lock:
            self._running = False
            self._idle = True
        _LOGGER.debug(f"Successfully set idle mode", self.__class__.__name__)

    def _unset_update(self):
        _LOGGER.debug("Unsetting %s.__update", self.__class__.__name__)
        with self.command_lock:
            self._update = False
        _LOGGER.debug("Unset of %s._update successful", self.__class__.__name__)

    def _execute_command(self, command: ICommand | ITaskHandler):
        with self.command_lock:
            self._executing = True
        command.execute()
        self._future = command.task
        # while self._executing:
        #     _sleep_msecs(self.COMMAND_LOOP_IDLE_SLEEP_MSECS)

    @_start_required
    def _command_loop(self):
        asyncio.set_event_loop(self._loop)
        _LOGGER.info("Command loop has started")
        while self._idle or self._running:
            while self._running and not self._idle:
                next_command = self.queue.get_next()
                # make into command object
                if next_command is None:
                    self._set_idle()
                    break
                self._current_command = next_command
                command = self._command_factory(next_command)
                self._execute_command(command) # synchronous for now

                _LOGGER.info("Waiting for command to complete execution...")
                loop = asyncio.get_event_loop()
                loop.run_forever() # until stop

                if self._pause_between_commands_msecs:
                    _sleep_msecs(self._pause_between_commands_msecs)
                if self._update:
                    self._unset_update()
            if not self._running and self._idle:
                _LOGGER.info("%s idle mode active. Waiting for commands...", self.__class__.__name__)
            i = 0
            while not self._running and self._idle:
                if i % 1000 == 0:
                    _LOGGER.debug("Command loop idling...")
                _sleep_msecs(i := i + self.COMMAND_LOOP_IDLE_SLEEP_MSECS)
        else:
            _LOGGER.info("Command loop stopping...")
            # any recourses need ta be freed and such
            ...

    def _command_factory(self, command_str) -> ICommand:
        _LOGGER.info(f"Creating Command from command string %s", command_str)
        command_split = command_str.split()
        command_name = command_split[0]
        command = NoCommand()
        # Probably need to do something about the literals
        match command_name:
            case "Move":
                command_type = MoveCommand
                arg_type = float
            case "Wait":
                command_type = WaitCommand
                arg_type = float
            case "SetOutput":
                command_type = SetOutputCommand
                arg_type = int
            case "ClearOutput":
                command_type = ClearOutputCommand
                arg_type = int
            case _:
                raise ValueError(f"Unexpected command {command_name}")

        command = command_type(self.motion_controller, *map(arg_type, command_split[1:]))
        _LOGGER.debug(f"Created command of type %s", command_type.__name__)
        self._commands_processed += 1

        return command


if __name__ == "__main__":
    cq = CommandQueue(["move"])
    mc = MotionController()
    cp = CommandProcessor(cq, mc)
    cp._command_loop()