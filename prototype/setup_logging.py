import yaml
from logging.config import dictConfig
import pathlib
import coloredlogs
import traceback

def setup_logging(logging_config="logging.yaml"):
    path_base = pathlib.Path(__file__).parent
    logconfig_path = path_base.joinpath(logging_config)
    if not logconfig_path.exists():
        raise FileNotFoundError(f"Could not find '{logging_config}'")
    logs_dir = path_base.joinpath("logs/")
    if not logs_dir.exists():
        logs_dir.mkdir()
    with open(logconfig_path, 'r') as f:
        try:
            config = yaml.safe_load(f.read())
            print(config)
            dictConfig(config)
            coloredlogs.install()
        except Exception as e:
            print(f"loading logging config unsuccessful: {e}")
            traceback.print_exc()
            raise
