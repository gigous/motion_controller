import pytest
from prototype import CommandProcessor, CommandQueue, MotionController
from prototype.prototype_main import _LOGGER

@pytest.fixture(scope="module")
def commands():
    return (
        "Move 3.5 6.3 1.0",
        "Wait 50",
        # "SetOutput 3",
        # "ClearOutput 1",
    )


@pytest.fixture(scope="function")
def queue(commands):
    cq = CommandQueue(commands)
    yield cq
    # cleanup


@pytest.fixture(scope="function")
def motion_controller():
    mc = MotionController()
    yield mc
    # cleanup


@pytest.fixture(scope="function")
def processor_no_controller(queue):
    cp = CommandProcessor(queue, None)
    yield cp
    _LOGGER.debug("======================== TEST COMPLETE ==========================")
    cp.stop_processing()
    clear_loggers()


@pytest.fixture(scope="function")
def processor(queue, motion_controller):
    cp = CommandProcessor(queue, motion_controller)
    yield cp
    cp.stop_processing()
    clear_loggers()

# https://github.com/pytest-dev/pytest/issues/5502
def clear_loggers():
    """Remove handlers from all loggers"""
    import logging
    loggers = [logging.getLogger()] + list(logging.Logger.manager.loggerDict.values())
    for logger in loggers:
        handlers = getattr(logger, 'handlers', [])
        for handler in handlers:
            logger.removeHandler(handler)
