from prototype import CommandProcessor, CommandQueue, ICommand, NoCommand, WaitCommand, MotionController
from prototype.prototype_main import _LOGGER
from threading import current_thread
import inspect
from time import sleep

def log_test_start(name):
    _LOGGER.debug("======================= TEST %s STARTING =======================", name)

class TestCommandProcessor:
    def test_tcp00_get_command(self, processor_no_controller: CommandProcessor):
        log_test_start(inspect.getframeinfo(inspect.currentframe()).function)
        cp: CommandProcessor = processor_no_controller
        cp.start_processing()
        while cp._running:
            sleep(0.01)
        assert cp._commands_processed == 4
        # for cmd in cmds:
        #     assert isinstance(cmd, ICommand) and not isinstance(cmd, NoCommand)
        # assert cmd == "Move"
        # cmd.execute()

    def test_tcp01_starts_again(self, queue: CommandQueue, processor_no_controller: CommandProcessor):
        log_test_start(inspect.getframeinfo(inspect.currentframe()).function)
        cq: CommandQueue = queue
        cp: CommandProcessor = processor_no_controller
        cp.start_processing()
        # TODO how to allow processor to finish processing?
        while cp._running:
            sleep(0.01)
        cq.add_more_commands(["Wait 543"])
        assert cp._commands_processed == 4
        while cp._running:
            sleep(0.01)
        assert cp._commands_processed == 5

        # cmds = cp.command_loop()
        # for cmd in cmds:
        #     assert isinstance(cmd, WaitCommand)
        #     assert cmd.msecs == 543.0
        # sleep(1)

        # while cp._running:
        #     pass
        # cp.stop_processing()

    def test_tcp02_before_start_throws(self, processor_no_controller):
        log_test_start(inspect.getframeinfo(inspect.currentframe()).function)
        cp: CommandProcessor = processor_no_controller
        try:
            cp.update()
            assert False
        except:
            assert cp._commands_processed == 0

    def test_tcp03_last_command_processed(self, processor_no_controller):
        log_test_start(inspect.getframeinfo(inspect.currentframe()).function)
        cp: CommandProcessor = processor_no_controller
        cp._pause_between_commands_msecs = 1000
        assert False # TODO


class TestMotionController:
    def test_tmc00_creation(self,  motion_controller: MotionController):
        log_test_start(inspect.getframeinfo(inspect.currentframe()).function)
        pass

    def test_tmc01_command_move(self,  motion_controller: MotionController):
        log_test_start(inspect.getframeinfo(inspect.currentframe()).function)
        motion_controller.move_to(0.3, 0.3, 0.3)
        assert motion_controller._executions['move_to'] == 1

    def test_tmc02_command_wait(self,  motion_controller: MotionController):
        log_test_start(inspect.getframeinfo(inspect.currentframe()).function)

    def test_tmc03_command_set_output(self,  motion_controller: MotionController):
        log_test_start(inspect.getframeinfo(inspect.currentframe()).function)

    def test_tmc04_command_clear_output(self,  motion_controller: MotionController):
        log_test_start(inspect.getframeinfo(inspect.currentframe()).function)

    def test_tmc05_command_fires(self, processor: CommandProcessor, motion_controller: MotionController):
        log_test_start(inspect.getframeinfo(inspect.currentframe()).function)
        processor.start_processing()
        while processor._running:
            sleep(0.01)
        assert motion_controller._executions["move_to"] == 1
        assert motion_controller._executions["dwell"] == 1
        assert processor._executing == False
