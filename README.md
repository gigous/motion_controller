# Motion Controller

This project was created for me to practice my skills in multi-threading, OOP, design patterns, and C++. It is implemented in two separate languages:

- Python, for the prototype, in [`prototype/`](/prototype/)
- C++, the final implementation, in [`cpp/`](/cpp/)

## Description

![System Diagram](img/system.png)

I defined on a high level what I wanted the motion controller to do. I assumed it was a simple controller that controlled 2 motors, one for the X direction, and one for the Y direction, and also had 8 digital outputs that could be set or cleared. The motion controller could do one of three things:

- Move - Move the CNC to an X and Y position from its current position at an overall speed of speed
- Dwell - Be idle for a specified number of milliseconds
- Set outputs - turn on or off one of 8 digital outputs

A command processor would be responsible for sending the motion controller any number of commands, one at a time, which it would receive from a command queue. The command processor would send the controller a command, then wait for a “finished” signal from the controller.
